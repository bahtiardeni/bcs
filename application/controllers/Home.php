<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
		!$this->access->is_login() ? redirect(base_url("auth/login/")) : "";
	}
	
	public function index()
	{
		$data["title"]		= "Dashboard";
		$data["page_id"]	= "page-home";
		
		$data["js"][]	= "asset/js/script.js";
		
		$data["_user"]	= $this->_user;
		$this->template->generate_template("home/index", $data);
	}
}
