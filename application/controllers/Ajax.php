<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Ajax
 *
 * @property Global_model 	$global_model
 * @property mylib 	        $mylib
 * @property hashids 	    $hashids
 * @property Input 	        $input
 */

class Ajax extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
	}
	
	function datatable()
	{
		$type			= $this->input->get("type");
		$page			= $this->input->get("page");
		$size			= (int)$this->input->get("size");
		$sort			= $this->input->get("sort");
		$search			= $this->input->get("search");
		$paged			= $page	== 1 ? 0 : (($page - 1) * 10);
		$link_uptede	= $this->input->get("link_update");
		
		$condition = array();
		
		if (!empty($search))
		{
			foreach($search as $field => $value)
			{
				//var_dump($field);
				$value = trim($value);
				
				if(strlen($value))
				{
					if ($type != "realisasi")
					{
						if ($type == "log_login")
						{
							$condition["login_history.".$field." LIKE "] = "%".$value."%";
						}
						elseif ($type == "audittrail")
						{
							$condition["audittrail.".$field." LIKE "] = "%".$value."%";
						}
						else
						{
							if ($field == "klasifikasi_id"){
								$field	= "mst_klasifikasi.".$field;
							}else if ($field == "role_id"){
								$field	= "mst_role.".$field;
							}else if ($field == "eselon_id"){
								$field	= "mst_eselon.".$field;
							}else if ($field == "ta_id"){
								$field	= "mst_ta.".$field;
							}else if ($field == "akun_id"){
								$field	= "mst_akun.".$field;
							}else if ($field == "satker_id"){
								$field	= "mst_satker.".$field;
							}
							
							if($field == "status"){
								$condition[$field] = $value;
							}
							else
								$condition[$field." LIKE "] = "%".$value."%";
						}
					}
					else
					{
						$condition[$field] = $value;
					}
				}
			}
		}
		
		$initial_table	= $this->initial_datatable($type);
		
		
		$table_name			= $initial_table["table_name"];
		$table_condition	= $condition;
		$table_order		= $initial_table["table_name"].".".$initial_table["sort_field"];
		$table_order_type	= $initial_table["sort_type"];
		$table_page			= $paged;
		$table_size			= $size;
		
		$table_select		= $initial_table["field"];
		$table_join			= $initial_table["table_join"];
		$table_where_in		= array();

        if (
            $type == "crew" ||
            $type == "section"
        ){
            $data	= $this->global_model->get_data($table_name, $table_condition, $table_order, $table_order_type, $table_page, $table_size, $table_select, $table_join, $table_where_in)->result();
            $count 	= $this->global_model->get_data($table_name, $table_condition, $table_order, $table_order_type, null, null, array(), $table_join, $table_where_in)->num_rows();
        }
		else
		{
			$data	= $this->global_model->get_data($initial_table["table_name"], $condition, $initial_table["table_name"].".".$initial_table["sort_field"], $initial_table["sort_type"], $paged, $size)->result();
			$count 	= $this->global_model->get_data($initial_table["table_name"], $condition)->num_rows();
		}
		
		$row_data		= array();
		
		foreach($data as $item_data)
		{
			$row = array();
			
			foreach($initial_table["field_table"] as $field)
			{
				$row[$field] = $item_data->$field == null ? "" : $item_data->$field;
			}
			
			$row["action"] = "
				<div class='btn-group'>
					<button data-toggle='dropdown' class='btn btn-info btn-xs dropdown-toggle' aria-expanded='false'>Action <span class='caret'></span></button>
					<ul class='dropdown-menu dropdown-menu-right'>
						<li><a href='".base_url($link_uptede.$this->hashids->encode($item_data->$initial_table["primary_key"]))."'><span class='fa fa-pencil-square'></span> Edit</a></li>
						<li><a href='javascript:void(0)' data-type='".$type."' data-id='".$this->hashids->encode($item_data->$initial_table["primary_key"])."' class='btn-remove-data'><span class='fa fa-trash'></span> Delete</a></li>
					</ul>
				</div>
			";
			
			$row_data[] = $row;
		}
		
		$json_data = array(
			"datatable"  => $row_data,
			"count"      => $count,
			"dataStart"  => $paged + 1,
			"dataSize"   => $size,
			"class"	 	 => $initial_table["class"],
			"function"	 => $initial_table["function"],
		);
		
		header('Content-Type: application/json');
		echo json_encode($json_data);
		
	}
	
	function datatable_update()
	{
		$type	= $this->input->post("type");
		$input	= $this->input->post("input");
		
		$initial_table	= $this->initial_datatable($type);
		
		$table_name		= $initial_table["table_name"];
		$primary_key	= $initial_table["primary_key"];
		$field_required	= $initial_table["field_required"];
		$field_unix     = !empty($initial_table["field_unix"]) ? $initial_table["field_unix"] : array();
		$table_field	= $initial_table["field_ori"];

		$result			= "error";
		$message		= "";
		
		$data_new		= array();
		
		foreach ($input as $field => $value)
		{
		    $field  = explode(".", $field);
		    $field  = !empty($field[1]) ? $field[1] : $field[0] ;

			$value	= trim($value);

			// Field Required
			if (array_key_exists($field, $field_required) && ($value == '' && $value !== 0 && $value !== '0'))
			{
				$message	.= "- ".$field_required[$field]." Tidak boleh kosong.<br>";
			}

			// Field Unix
            if (array_key_exists($field, $field_unix) && !empty($value))
            {
                $field_id_tmp	= $this->hashids->decode($input[$primary_key]);
                $field_id_tmp	= !empty($field_id_tmp[0]) ? $field_id_tmp[0] : null;

                if (empty($field_id_tmp))
                {
                    $check_data = $this->global_model->get_data($table_name, array(
                        $field => $value
                    ))->num_rows();
                }
                else
                {
                    $check_data = $this->global_model->get_data($table_name, array(
                        $field       => $value,
                        $primary_key." != " => $field_id_tmp
                    ))->num_rows();
                }

                /*echo $this->db->last_query();
                var_dump($check_data);
                die();*/

                if (!empty($check_data)){
                    $message	.= "- ".$field_unix[$field]." \"".$value."\" telah ada.<br>";
                }

            }
			
			if (in_array($field, $table_field))
			{
				if ($field == $primary_key)
				{
					if (empty($value)){
						$field_id	= "";
					}else{
						$field_id	= $this->hashids->decode($value);
						$field_id	= $field_id[0];
					}
					
					$data_new[$field]	= $field_id;
				}
				else if ($field == "password")
				{
					if ((!empty($input[$primary_key]) && $value != $input["old_password"]) || empty($input[$primary_key])){
						$data_new[$field]	= sha1(md5(trim($value)));
					}
					else
					{
						$data_new[$field]	= $value;
					}
				}
				else
				{
					$data_new[$field]	= $value;
				}
			}
		}
		
		if (empty($message))
		{
			$data_new["updated_on"]	= date("Y-m-d H:i:s");
			$data_new["updated_by"]	= $this->_user->crew_name;

			if (empty($data_new[$primary_key]))
			{
				$old_data   = array();

				$data_new["created_on"]	= date("Y-m-d H:i:s");
				$data_new["created_by"]	= $this->_user->crew_name;

                if ($type == "crew")
                {
                    $data_new["crew_password"]   = md5($data_new["crew_password"]);
                }

				$data_id = $this->global_model->save($table_name, $data_new);
			}
			else
			{
				$old_data   = $this->global_model->get_data($table_name, array(
					$primary_key => $data_new[$primary_key]
				))->row();

				if ($type == "crew")
				{
				    if ($data_new["crew_password"] != $old_data->crew_password)
                    {
                        $data_new["crew_password"]   = md5($data_new["crew_password"]);
                    }
				}
				
				$this->global_model->update($table_name, array(
					$primary_key => $data_new[$primary_key]
				), $data_new);
				
				$data_id 	= $data_new[$primary_key];
			}
			
			$result		= "success";
			$message	= "Data berhasil disimpan.";
			
			
			$this->session->set_flashdata(array(
				"message_success"	=> $message
			));
		}
		
		echo json_encode(array(
			"result"	=> $result,
			"message"	=> $message
		));
		
		die();
	}

    function datatable_delete()
    {
        $type		= trim($this->input->post("type"));
        $tmp_hash	= trim($this->input->post("id"));
        $tmp_hash	= $this->hashids->decode($tmp_hash);
        $id			= !empty($tmp_hash[0]) ? $tmp_hash[0] : null;
        $result		= "error";
        $message	= "";

        $initial_table	= $this->initial_datatable($type);

        if (!empty($id))
        {
            if ($type == "budget")
            {
                $cek_data	= $this->global_model->get_data("vw_realisasi", array(
                    "budget_id" => $id
                ))->row();

                if (empty($cek_data->total)){
                    $delete	= $this->global_model->delete($initial_table["table_name"], array(
                        $initial_table["primary_key"]	=> $id
                    ));

                    if ($delete){
                        $result		= "success";
                        $message	= "Data has been deleted.";
                    }else{
                        $message	= "<span class='fa fa-info-circle'></span>&nbsp; Data gagal di hapus karena masih digunakan.";
                    }
                }else{
                    $message	= "<span class='fa fa-info-circle'></span>&nbsp; Data gagal di hapus karena telah digunakan.";
                }
            }
            else
            {
                $delete	= $this->global_model->delete($initial_table["table_name"], array(
                    $initial_table["primary_key"]	=> $id
                ));

                if ($delete){
                    $result		= "success";
                    $message	= "Data berhasil dihapus.";
                }else{
                    $message	= "<span class='fa fa-info-circle'></span>&nbsp; Data gagal di hapus karena masih digunakan.";
                }
            }


        }

        echo json_encode(array(
            "result"	=> $result,
            "message"	=> $message
        ));

        die();
    }
	
	function initial_datatable($type)
	{
		$return	= array();
		$return["number_format"]	= array();
		
		if ($type == "crew")
		{
			$return	= array(
				"table_name"		=> "mst_crew",
				"primary_key"		=> "crew_id",
				"field_ori"			=> array("crew_id", "crew_name", "crew_password", "crew_email", "crew_nik", "crew_hp", "crew_sections_id", "crew_avatar", "created_by", "created_on", "updated_by", "updated_on"),
				"field"				=> array("crew_id", "crew_name", "crew_password", "crew_email", "crew_nik", "crew_hp", "crew_sections_id", "crew_avatar", "created_by", "created_on", "updated_by", "updated_on"),
				"field_table"		=> array("section_name","crew_nik","crew_email","crew_name","crew_hp", "created_on", "created_by", "updated_on", "updated_by"),
				"sort_field"		=> "created_on",
				"sort_type"			=> "DESC",
				"class"				=> array(
					"text-center"	=> array("created_on","updated_on", "crew_hp"),
					"text-right"	=> array(),
					"hidden-xs"		=> array("section_name","crew_hp", "created_on", "created_by", "updated_on", "updated_by"),
				),
				"function"			=> array(
					"number"		=> array(),
				),
				
				"field_required" 	=> array("crew_sections_id" => "Section"),
				"table_join"		=> array(
					"JOIN" => array(
						"mst_section" => "mst_section.section_id = mst_crew.crew_sections_id"
					)
				),
				"table_join_select"	=> array("mst_section.section_name"),
			);
		}
		else if ($type == "department")
        {
            $return	= array(
                "table_name"		=> "mst_department",
                "primary_key"		=> "dept_id",
                "field_ori"			=> array("dept_id", "dept_name", "created_by", "created_on", "updated_by", "updated_on"),
                "field"				=> array("dept_id", "dept_name", "created_by", "created_on", "updated_by", "updated_on"),
                "field_table"		=> array("dept_name", "created_on", "created_by", "updated_on", "updated_by"),
                "sort_field"		=> "created_on",
                "sort_type"			=> "DESC",
                "class"				=> array(
                    "text-center"	=> array("created_on","updated_on", "crew_hp"),
                    "text-right"	=> array(),
                    "hidden-xs"		=> array("created_on", "created_by", "updated_on", "updated_by"),
                ),
                "function"			=> array(
                    "number"		=> array(),
                ),

                "field_required" 	=> array("dept_name" => "Department Name"),
                "field_unix"        => array("dept_name" => "Department Name"),
                "table_join"		=> array(),
                "table_join_select"	=> array(),
            );
        }
        else if ($type == "section")
        {
            $return	= array(
                "table_name"		=> "mst_section",
                "primary_key"		=> "section_id",
                "field_ori"			=> array("section_id", "section_name", "dept_id", "created_by", "created_on", "updated_by", "updated_on"),
                "field"				=> array("section_id", "section_name", "dept_id", "created_by", "created_on", "updated_by", "updated_on"),
                "field_table"		=> array("dept_name", "section_name", "created_on", "created_by", "updated_on", "updated_by"),
                "sort_field"		=> "created_on",
                "sort_type"			=> "DESC",
                "class"				=> array(
                    "text-center"	=> array("created_on","updated_on", "section_hp"),
                    "text-right"	=> array(),
                    "hidden-xs"		=> array("created_on", "created_by", "updated_on", "updated_by"),
                ),
                "function"			=> array(
                    "number"		=> array(),
                ),

                "field_required" 	=> array(
                    "section_name" => "Section Name",
                    "dept_id" => "Department"
                ),
                "field_unix" 	    => array("section_name" => "Section Name"),
                "table_join"		=> array(
                    "JOIN" => array(
                        "mst_department" => "mst_department.dept_id = mst_section.dept_id"
                    )
                ),
                "table_join_select"	=> array("mst_department.dept_name"),
            );
        }
        else if ($type == "shift")
        {
            $return	= array(
                "table_name"		=> "mst_shift",
                "primary_key"		=> "shift_id",
                "field_ori"			=> array("shift_id", "shift_name", "shift_in", "shift_out", "remark", "created_by", "created_on", "updated_by", "updated_on"),
                "field"				=> array("shift_id", "shift_name", "shift_in", "shift_out", "remark", "created_by", "created_on", "updated_by", "updated_on"),
                "field_table"		=> array("shift_name", "shift_in", "shift_out", "remark", "created_on", "created_by", "updated_on", "updated_by"),
                "sort_field"		=> "created_on",
                "sort_type"			=> "DESC",
                "class"				=> array(
                    "text-center"	=> array("shift_in", "shift_out", "created_on","updated_on"),
                    "text-right"	=> array(),
                    "hidden-xs"		=> array("created_on", "created_by", "updated_on", "updated_by"),
                ),
                "function"			=> array(
                    "number"		=> array(),
                ),

                "field_required" 	=> array(
                    "shift_name" => "Shift Name",
                    "shift_in"   => "Shift In",
                    "shift_out"  => "Shift Out"
                ),
                "field_unix" 	    => array("shift_name" => "Shift Name"),
                "table_join"		=> array(),
                "table_join_select"	=> array(),
            );
        }
        else if ($type == "activity")
        {
            $return	= array(
                "table_name"		=> "mst_activity",
                "primary_key"		=> "activity_id",
                "field_ori"			=> array("activity_id", "activity_name", "created_by", "created_on", "updated_by", "updated_on"),
                "field"				=> array("activity_id", "activity_name", "created_by", "created_on", "updated_by", "updated_on"),
                "field_table"		=> array("activity_name", "created_on", "created_by", "updated_on", "updated_by"),
                "sort_field"		=> "created_on",
                "sort_type"			=> "DESC",
                "class"				=> array(
                    "text-center"	=> array("created_on","updated_on", "crew_hp"),
                    "text-right"	=> array(),
                    "hidden-xs"		=> array("created_on", "created_by", "updated_on", "updated_by"),
                ),
                "function"			=> array(
                    "number"		=> array(),
                ),

                "field_required" 	=> array("activity_name" => "Activity Name"),
                "field_unix" 	    => array("activity_name" => "Activity Name"),
                "table_join"		=> array(),
                "table_join_select"	=> array(),
            );
        }
        else if ($type == "location")
        {
            $return	= array(
                "table_name"		=> "mst_location",
                "primary_key"		=> "location_id",
                "field_ori"			=> array("location_id", "location_name", "created_by", "created_on", "updated_by", "updated_on"),
                "field"				=> array("location_id", "location_name", "created_by", "created_on", "updated_by", "updated_on"),
                "field_table"		=> array("location_name", "created_on", "created_by", "updated_on", "updated_by"),
                "sort_field"		=> "created_on",
                "sort_type"			=> "DESC",
                "class"				=> array(
                    "text-center"	=> array("created_on","updated_on", "crew_hp"),
                    "text-right"	=> array(),
                    "hidden-xs"		=> array("created_on", "created_by", "updated_on", "updated_by"),
                ),
                "function"			=> array(
                    "number"		=> array(),
                ),

                "field_required" 	=> array("location_name" => "Location Name"),
                "field_unix" 	    => array("location_name" => "Location Name"),
                "table_join"		=> array(),
                "table_join_select"	=> array(),
            );
        }
        else if ($type == "ratio")
        {
            $return	= array(
                "table_name"		=> "mst_aratio",
                "primary_key"		=> "aratio_id",
                "field_ori"			=> array("aratio_id", "aratio_name", "created_by", "created_on", "updated_by", "updated_on"),
                "field"				=> array("aratio_id", "aratio_name", "created_by", "created_on", "updated_by", "updated_on"),
                "field_table"		=> array("aratio_name", "created_on", "created_by", "updated_on", "updated_by"),
                "sort_field"		=> "created_on",
                "sort_type"			=> "DESC",
                "class"				=> array(
                    "text-center"	=> array("created_on","updated_on", "crew_hp"),
                    "text-right"	=> array(),
                    "hidden-xs"		=> array("created_on", "created_by", "updated_on", "updated_by"),
                ),
                "function"			=> array(
                    "number"		=> array(),
                ),

                "field_required" 	=> array("aratio_name" => "Aspek Ratio Name"),
                "field_unix" 	    => array("aratio_name" => "Aspek Ratio Name"),
                "table_join"		=> array(),
                "table_join_select"	=> array(),
            );
        }
        else if ($type == "source")
        {
            $return	= array(
                "table_name"		=> "mst_source",
                "primary_key"		=> "source_id",
                "field_ori"			=> array("source_id", "source_name", "created_by", "created_on", "updated_by", "updated_on"),
                "field"				=> array("source_id", "source_name", "created_by", "created_on", "updated_by", "updated_on"),
                "field_table"		=> array("source_name", "created_on", "created_by", "updated_on", "updated_by"),
                "sort_field"		=> "created_on",
                "sort_type"			=> "DESC",
                "class"				=> array(
                    "text-center"	=> array("created_on","updated_on", "crew_hp"),
                    "text-right"	=> array(),
                    "hidden-xs"		=> array("created_on", "created_by", "updated_on", "updated_by"),
                ),
                "function"			=> array(
                    "number"		=> array(),
                ),

                "field_required" 	=> array("source_name" => "Source Name"),
                "field_unix" 	    => array("source_name" => "Source Name"),
                "table_join"		=> array(),
                "table_join_select"	=> array(),
            );
        }
        else if ($type == "status")
        {
            $return	= array(
                "table_name"		=> "mst_status",
                "primary_key"		=> "status_id",
                "field_ori"			=> array("status_id", "status_name", "created_by", "created_on", "updated_by", "updated_on"),
                "field"				=> array("status_id", "status_name", "created_by", "created_on", "updated_by", "updated_on"),
                "field_table"		=> array("status_name", "created_on", "created_by", "updated_on", "updated_by"),
                "sort_field"		=> "created_on",
                "sort_type"			=> "DESC",
                "class"				=> array(
                    "text-center"	=> array("created_on","updated_on", "crew_hp"),
                    "text-right"	=> array(),
                    "hidden-xs"		=> array("created_on", "created_by", "updated_on", "updated_by"),
                ),
                "function"			=> array(
                    "number"		=> array(),
                ),

                "field_required" 	=> array("status_name" => "Status Name"),
                "field_unix" 	    => array("status_name" => "Status Name"),
                "table_join"		=> array(),
                "table_join_select"	=> array(),
            );
        }
        else if ($type == "action")
        {
            $return	= array(
                "table_name"		=> "mst_action",
                "primary_key"		=> "action_id",
                "field_ori"			=> array("action_id", "action_name", "created_by", "created_on", "updated_by", "updated_on"),
                "field"				=> array("action_id", "action_name", "created_by", "created_on", "updated_by", "updated_on"),
                "field_table"		=> array("action_name", "created_on", "created_by", "updated_on", "updated_by"),
                "sort_field"		=> "created_on",
                "sort_type"			=> "DESC",
                "class"				=> array(
                    "text-center"	=> array("created_on","updated_on", "crew_hp"),
                    "text-right"	=> array(),
                    "hidden-xs"		=> array("created_on", "created_by", "updated_on", "updated_by"),
                ),
                "function"			=> array(
                    "number"		=> array(),
                ),

                "field_required" 	=> array("action_name" => "Action Name"),
                "field_unix" 	    => array("action_name" => "Action Name"),
                "table_join"		=> array(),
                "table_join_select"	=> array(),
            );
        }

		foreach ($return["field"] as $key => $field)
		{
			$return["field"][$key]	= $return["table_name"].".".$field;
		}
		
		$return["field"]	= array_merge($return["field"], $return["table_join_select"]);
		
		return $return;
	}
	
	function upload_images($type)
	{
		if ($type == "avatar")
        {
            $path       = "/asset/images/avatar";
            $path_ex    = explode("/", $path);
            $path       = $this->mylib->create_dir(array_filter($path_ex), $path);
        }

        $config['upload_path'] 		= './asset/upload';
		$config['allowed_types'] 	= 'jpg|jpeg|gif|bmp|jpe|png';
		$this->load->library('upload', $config);

		if ($this->upload->do_upload('file')) {
			$file_data = $this->upload->data();
			
			// get file hash
			$temp_file = $file_data['full_path'];
			$file_hash = md5_file($temp_file);
			$target_dir = '.'.$path;
			$target_file = $target_dir . DIRECTORY_SEPARATOR . $file_hash . $file_data['file_ext'];
			
			if (file_exists($target_file)) {
				unlink($temp_file);
			} else {
				rename($temp_file, $target_file);
			}

			
			echo json_encode(array(
				"result"    => "success",
				"file" 		=> $file_hash . $file_data['file_ext'],
				"path"      => $this->_path_avatar.DIRECTORY_SEPARATOR.$file_hash . $file_data['file_ext']
			));
		}else{
        }
		
		return false;
	}
	
}