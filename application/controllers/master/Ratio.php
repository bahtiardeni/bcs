<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 *
 * @property Global_model 	$global_model
 * @property Hashids 		$hashids
 * @property Formlib 		$formlib
 */

class Ratio extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        !$this->access->is_login() ? redirect(base_url("auth/login/")) : "";
    }

    public function index()
    {
        $data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
        $data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
        $data["js"][]	= "asset/js/script.js";

        $data["datatable"]		= "ratio";
        $data["link_update"]	= "master/ratio/update/";

        $data["title"]		= "Manage Data Aspek Ratio";
        $data["page_id"]	= "page-ratio";
        $data["_user"]		= $this->_user;
        $this->template->generate_template("master/ratio/index", $data);
    }

    public function update($id_hash = null)
    {
        $tmp_hash	= $this->hashids->decode($id_hash);
        $id			= !empty($tmp_hash[0]) ? $tmp_hash[0] : null;

        $data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
        //$data["css"][]	= "asset/template/plugins/timepicker/bootstrap-timepicker.min.css";
        $data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
        //$data["js"][]	= "asset/template/plugins/timepicker/bootstrap-timepicker.min.js";
        $data["js"][]	= "asset/js/script.js";

        if (empty($id))
        {
            $title	= "Add Data Aspek Ratio";

            $aratio_id	= "";
            $aratio_name  = "";
        }
        else
        {
            $edit	= $this->global_model->get_data("mst_aratio", array(
                "aratio_id"	=> $id
            ))->row();

            $title			= "Edit Data Aspek Ratio";
            $aratio_id	    = $this->hashids->encode($edit->aratio_id);
            $aratio_name	= $edit->aratio_name;
        }

        $data["type"]					= $this->formlib->_generate_input_text("datatable_type", "type", "datatable", "ratio", "hidden");

        $data["input"]["aratio_id"]  	= $this->formlib->_generate_input_text("aratio_id", "input[aratio_id]", "ratio ID", $aratio_id, "hidden");
        $data["input"]["aratio_name"]  	= $this->formlib->_generate_input_text("aratio_name", "input[aratio_name]", "Aspel Ratio", $aratio_name);

        $data["link_back"]	= base_url("master/ratio/");

        $data["title"]		= $title;
        $data["page_id"]	= "page-ratio-update";
        $data["_user"]		= $this->_user;
        $this->template->generate_template("master/ratio/update", $data);
    }
}
