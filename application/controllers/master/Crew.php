<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 *
 * @property Global_model 	$global_model
 * @property Hashids 		$hashids
 * @property Formlib 		$formlib
 */

class Crew extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
		!$this->access->is_login() ? redirect(base_url("auth/login/")) : "";
	}
	
	public function index()
	{
        $data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
        $data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
		$data["js"][]	= "asset/js/script.js";
		
		$data["department"]		= $this->global_model->get_data("mst_department")->result();
		$data["datatable"]		= "crew";
		$data["link_update"]	= "master/crew/update/";
		
		$data["title"]		= "Manage Data Crew";
		$data["page_id"]	= "page-crew";
		$data["_user"]		= $this->_user;
		$this->template->generate_template("master/crew/index", $data);
	}
	
	public function update($id_hash = null)
	{
		$tmp_hash	= $this->hashids->decode($id_hash);
		$id			= !empty($tmp_hash[0]) ? $tmp_hash[0] : null;
		
		$data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
		$data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
		$data["js"][]	= "asset/js/script.js";
		
		if (empty($id))
		{
			$title	= "Add Data Crew";
			
			$crew_id			= "";
			$crew_name			= "";
			$crew_password		= "";
			$crew_email			= "";
			$crew_nik			= "";
			$crew_hp			= "";
			$crew_sections_id	= "";
			$crew_avatar		= "default.jpg";

			
			
		}
		else
		{
			$edit	= $this->global_model->get_data("mst_crew", array(
				"crew_id"	=> $id
			))->row();
			
			$title				= "Edit Data Crew";
			$crew_id			= $this->hashids->encode($edit->crew_id);
			$crew_name			= $edit->crew_name;
			$crew_password		= $edit->crew_password;
			$crew_email			= $edit->crew_email;
			$crew_nik			= $edit->crew_nik;
			$crew_hp			= $edit->crew_hp;
			$crew_sections_id	= $edit->crew_sections_id;
			$crew_avatar		= $edit->crew_avatar;
		}
		
		$data["type"]				= $this->formlib->_generate_input_text("datatable_type", "type", "datatable", "crew", "hidden");
		
		
		$data["input"]["crew_id"]  			= $this->formlib->_generate_input_text("crew_id", "input[crew_id]", "Crew ID", $crew_id , "hidden");
		$data["input"]["crew_name"]  		= $this->formlib->_generate_input_text("crew_name", "input[crew_name]", "Name", $crew_name);
		$data["input"]["crew_password"]  	= $this->formlib->_generate_input_text("crew_password", "input[crew_password]", "Password", $crew_password, "password");
		$data["input"]["crew_email"]  		= $this->formlib->_generate_input_text("crew_email", "input[crew_email]", "Email", $crew_email);
		$data["input"]["crew_nik"]  		= $this->formlib->_generate_input_text("crew_nik", "input[crew_nik]", "NIK", $crew_nik);
		$data["input"]["crew_hp"]  			= $this->formlib->_generate_input_text("crew_hp", "input[crew_hp]", "HP", $crew_hp);
		$data["input"]["crew_avatar"]  		= $this->formlib->_generate_input_file_image("crew_avatar", "input[crew_avatar]", $crew_avatar, $this->_path_avatar);
		$data["input"]["crew_sections_id"]  = $this->formlib->_generate_dropdown_table("mst_section", array(), "section_id", "section_name", "crew_sections_id","input[crew_sections_id]", $crew_sections_id);
		
		$data["link_back"]	= base_url("master/crew/");
		
		$data["title"]		= $title;
		$data["page_id"]	= "page-crew-update";
		$data["_user"]		= $this->_user;
		$this->template->generate_template("master/crew/update", $data);
	}
}
