<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 *
 * @property Global_model 	$global_model
 * @property Hashids 		$hashids
 * @property Formlib 		$formlib
 */

class Shift extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        !$this->access->is_login() ? redirect(base_url("auth/login/")) : "";
    }

    public function index()
    {
        $data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
        $data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
        $data["js"][]	= "asset/js/script.js";

        $data["section"]		= $this->global_model->get_data("mst_shift")->result();
        $data["datatable"]		= "shift";
        $data["link_update"]	= "master/shift/update/";

        $data["title"]		= "Manage Data Shift Schedule";
        $data["page_id"]	= "page-shift";
        $data["_user"]		= $this->_user;
        $this->template->generate_template("master/shift/index", $data);
    }

    public function update($id_hash = null)
    {
        $tmp_hash	= $this->hashids->decode($id_hash);
        $id			= !empty($tmp_hash[0]) ? $tmp_hash[0] : null;

        $data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
        //$data["css"][]	= "asset/template/plugins/timepicker/bootstrap-timepicker.min.css";
        $data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
        //$data["js"][]	= "asset/template/plugins/timepicker/bootstrap-timepicker.min.js";
        $data["js"][]	= "asset/js/script.js";

        if (empty($id))
        {
            $title	= "Add Data Shift Schedule";

            $shift_id	= "";
            $shift_name = "";
            $shift_in   = "";
            $shift_out	= "";
            $remark 	= "";
        }
        else
        {
            $edit	= $this->global_model->get_data("mst_shift", array(
                "shift_id"	=> $id
            ))->row();

            $title			= "Edit Data Shift Schedule";
            $shift_id		= $this->hashids->encode($edit->shift_id);
            $shift_name	    = $edit->shift_name;
            $shift_in       = $edit->shift_in;
            $shift_out	    = $edit->shift_out;
            $remark 	    = $edit->remark;
        }

        $data["type"]           = $this->formlib->_generate_input_text("datatable_type", "type", "datatable", "shift", "hidden");

        $data["input"]["shift_id"]  	= $this->formlib->_generate_input_text("shift_id", "input[shift_id]", "Shift ID", $shift_id, "hidden");
        $data["input"]["shift_name"]  	= $this->formlib->_generate_input_text("shift_name", "input[shift_name]", "Shift Name", $shift_name);
        $data["input"]["shift_in"]  	= $this->formlib->_generate_input_time("shift_in", "input[shift_in]", "Shift In", $shift_in);
        $data["input"]["shift_out"]  	= $this->formlib->_generate_input_time("shift_out", "input[shift_out]", "Shift Out", $shift_out);
        $data["input"]["remark"]  	    = $this->formlib->_generate_input_text("remark", "input[remark]", "Remark", $remark);

        $data["link_back"]	= base_url("master/shift/");

        $data["title"]		= $title;
        $data["page_id"]	= "page-shift-update";
        $data["_user"]		= $this->_user;
        $this->template->generate_template("master/shift/update", $data);
    }
}
