<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 *
 * @property Global_model 	$global_model
 * @property Hashids 		$hashids
 * @property Formlib 		$formlib
 */

class Source extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        !$this->access->is_login() ? redirect(base_url("auth/login/")) : "";
    }

    public function index()
    {
        $data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
        $data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
        $data["js"][]	= "asset/js/script.js";

        $data["datatable"]		= "source";
        $data["link_update"]	= "master/source/update/";

        $data["title"]		= "Manage Data Source";
        $data["page_id"]	= "page-source";
        $data["_user"]		= $this->_user;
        $this->template->generate_template("master/source/index", $data);
    }

    public function update($id_hash = null)
    {
        $tmp_hash	= $this->hashids->decode($id_hash);
        $id			= !empty($tmp_hash[0]) ? $tmp_hash[0] : null;

        $data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
        //$data["css"][]	= "asset/template/plugins/timepicker/bootstrap-timepicker.min.css";
        $data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
        //$data["js"][]	= "asset/template/plugins/timepicker/bootstrap-timepicker.min.js";
        $data["js"][]	= "asset/js/script.js";

        if (empty($id))
        {
            $title	= "Add Data Source";

            $source_id	= "";
            $source_name  = "";
        }
        else
        {
            $edit	= $this->global_model->get_data("mst_source", array(
                "source_id"	=> $id
            ))->row();

            $title			= "Edit Data Source";
            $source_id	    = $this->hashids->encode($edit->source_id);
            $source_name	= $edit->source_name;
        }

        $data["type"]					= $this->formlib->_generate_input_text("datatable_type", "type", "datatable", "source", "hidden");

        $data["input"]["source_id"]  	= $this->formlib->_generate_input_text("source_id", "input[source_id]", "source ID", $source_id, "hidden");
        $data["input"]["source_name"]  	= $this->formlib->_generate_input_text("source_name", "input[source_name]", "Source Name", $source_name);

        $data["link_back"]	= base_url("master/source/");

        $data["title"]		= $title;
        $data["page_id"]	= "page-source-update";
        $data["_user"]		= $this->_user;
        $this->template->generate_template("master/source/update", $data);
    }
}
