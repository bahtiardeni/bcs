<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 *
 * @property Global_model 	$global_model
 * @property Hashids 		$hashids
 * @property Formlib 		$formlib
 */

class Location extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        !$this->access->is_login() ? redirect(base_url("auth/login/")) : "";
    }

    public function index()
    {
        $data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
        $data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
        $data["js"][]	= "asset/js/script.js";

        $data["datatable"]		= "location";
        $data["link_update"]	= "master/location/update/";

        $data["title"]		= "Manage Data Location";
        $data["page_id"]	= "page-location";
        $data["_user"]		= $this->_user;
        $this->template->generate_template("master/location/index", $data);
    }

    public function update($id_hash = null)
    {
        $tmp_hash	= $this->hashids->decode($id_hash);
        $id			= !empty($tmp_hash[0]) ? $tmp_hash[0] : null;

        $data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
        //$data["css"][]	= "asset/template/plugins/timepicker/bootstrap-timepicker.min.css";
        $data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
        //$data["js"][]	= "asset/template/plugins/timepicker/bootstrap-timepicker.min.js";
        $data["js"][]	= "asset/js/script.js";

        if (empty($id))
        {
            $title	= "Add Data Location";

            $location_id	= "";
            $location_name  = "";
        }
        else
        {
            $edit	= $this->global_model->get_data("mst_location", array(
                "location_id"	=> $id
            ))->row();

            $title			= "Edit Data Location";
            $location_id	= $this->hashids->encode($edit->location_id);
            $location_name	= $edit->location_name;
        }

        $data["type"]                       = $this->formlib->_generate_input_text("datatable_type", "type", "datatable", "location", "hidden");

        $data["input"]["location_id"]  	    = $this->formlib->_generate_input_text("location_id", "input[location_id]", "location ID", $location_id, "hidden");
        $data["input"]["location_name"]  	= $this->formlib->_generate_input_text("location_name", "input[location_name]", "location Name", $location_name);

        $data["link_back"]	= base_url("master/location/");

        $data["title"]		= $title;
        $data["page_id"]	= "page-location-update";
        $data["_user"]		= $this->_user;
        $this->template->generate_template("master/location/update", $data);
    }
}
