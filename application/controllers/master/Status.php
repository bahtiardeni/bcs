<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 *
 * @property Global_model 	$global_model
 * @property Hashids 		$hashids
 * @property Formlib 		$formlib
 */

class Status extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        !$this->access->is_login() ? redirect(base_url("auth/login/")) : "";
    }

    public function index()
    {
        $data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
        $data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
        $data["js"][]	= "asset/js/script.js";

        $data["datatable"]		= "status";
        $data["link_update"]	= "master/status/update/";

        $data["title"]		= "Manage Data Status";
        $data["page_id"]	= "page-status";
        $data["_user"]		= $this->_user;
        $this->template->generate_template("master/status/index", $data);
    }

    public function update($id_hash = null)
    {
        $tmp_hash	= $this->hashids->decode($id_hash);
        $id			= !empty($tmp_hash[0]) ? $tmp_hash[0] : null;

        $data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
        //$data["css"][]	= "asset/template/plugins/timepicker/bootstrap-timepicker.min.css";
        $data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
        //$data["js"][]	= "asset/template/plugins/timepicker/bootstrap-timepicker.min.js";
        $data["js"][]	= "asset/js/script.js";

        if (empty($id))
        {
            $title	= "Add Data Status";

            $status_id	= "";
            $status_name  = "";
        }
        else
        {
            $edit	= $this->global_model->get_data("mst_status", array(
                "status_id"	=> $id
            ))->row();

            $title			= "Edit Data Status";
            $status_id	    = $this->hashids->encode($edit->status_id);
            $status_name	= $edit->status_name;
        }

        $data["type"]					= $this->formlib->_generate_input_text("datatable_type", "type", "datatable", "status", "hidden");

        $data["input"]["status_id"]  	= $this->formlib->_generate_input_text("status_id", "input[status_id]", "status ID", $status_id, "hidden");
        $data["input"]["status_name"]  	= $this->formlib->_generate_input_text("status_name", "input[status_name]", "Aspel status", $status_name);

        $data["link_back"]	= base_url("master/status/");

        $data["title"]		= $title;
        $data["page_id"]	= "page-status-update";
        $data["_user"]		= $this->_user;
        $this->template->generate_template("master/status/update", $data);
    }
}
