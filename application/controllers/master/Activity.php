<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 *
 * @property Global_model 	$global_model
 * @property Hashids 		$hashids
 * @property Formlib 		$formlib
 */

class Activity extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        !$this->access->is_login() ? redirect(base_url("auth/login/")) : "";
    }

    public function index()
    {
        $data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
        $data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
        $data["js"][]	= "asset/js/script.js";

        $data["datatable"]		= "activity";
        $data["link_update"]	= "master/activity/update/";

        $data["title"]		= "Manage Data Activity";
        $data["page_id"]	= "page-activity";
        $data["_user"]		= $this->_user;
        $this->template->generate_template("master/activity/index", $data);
    }

    public function update($id_hash = null)
    {
        $tmp_hash	= $this->hashids->decode($id_hash);
        $id			= !empty($tmp_hash[0]) ? $tmp_hash[0] : null;

        $data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
        //$data["css"][]	= "asset/template/plugins/timepicker/bootstrap-timepicker.min.css";
        $data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
        //$data["js"][]	= "asset/template/plugins/timepicker/bootstrap-timepicker.min.js";
        $data["js"][]	= "asset/js/script.js";

        if (empty($id))
        {
            $title	= "Add Data Activity";

            $activity_id	= "";
            $activity_name  = "";
        }
        else
        {
            $edit	= $this->global_model->get_data("mst_activity", array(
                "activity_id"	=> $id
            ))->row();

            $title			= "Edit Data Activity";
            $activity_id	= $this->hashids->encode($edit->activity_id);
            $activity_name	= $edit->activity_name;
        }

        $data["type"]                       = $this->formlib->_generate_input_text("datatable_type", "type", "datatable", "activity", "hidden");

        $data["input"]["activity_id"]  	    = $this->formlib->_generate_input_text("activity_id", "input[activity_id]", "activity ID", $activity_id, "hidden");
        $data["input"]["activity_name"]  	= $this->formlib->_generate_input_text("activity_name", "input[activity_name]", "activity Name", $activity_name);

        $data["link_back"]	= base_url("master/activity/");

        $data["title"]		= $title;
        $data["page_id"]	= "page-activity-update";
        $data["_user"]		= $this->_user;
        $this->template->generate_template("master/activity/update", $data);
    }
}
