<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 *
 * @property Global_model 	$global_model
 * @property Hashids 		$hashids
 * @property Formlib 		$formlib
 */

class Section extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        !$this->access->is_login() ? redirect(base_url("auth/login/")) : "";
    }

    public function index()
    {
        $data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
        $data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
        $data["js"][]	= "asset/js/script.js";

        $data["section"]		= $this->global_model->get_data("mst_section")->result();
        $data["datatable"]		= "section";
        $data["link_update"]	= "master/section/update/";

        $data["title"]		= "Manage Data Section";
        $data["page_id"]	= "page-section";
        $data["_user"]		= $this->_user;
        $this->template->generate_template("master/section/index", $data);
    }

    public function update($id_hash = null)
    {
        $tmp_hash	= $this->hashids->decode($id_hash);
        $id			= !empty($tmp_hash[0]) ? $tmp_hash[0] : null;

        $data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
        $data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
        $data["js"][]	= "asset/js/script.js";

        if (empty($id))
        {
            $title	= "Add Data Section";

            $section_id			= "";
            $dept_id			= "";
            $section_name		= "";
        }
        else
        {
            $edit	= $this->global_model->get_data("mst_section", array(
                "section_id"	=> $id
            ))->row();

            $title				= "Edit Data Section";
            $section_id			= $this->hashids->encode($edit->section_id);
            $dept_id			= $edit->dept_id;
            $section_name		= $edit->section_name;
        }

        $data["type"]				= $this->formlib->_generate_input_text("datatable_type", "type", "datatable", "section", "hidden");

        $data["input"]["section_id"]  		= $this->formlib->_generate_input_text("section_id", "input[section_id]", "section ID", $section_id , "hidden");
        $data["input"]["dept_id"]           = $this->formlib->_generate_dropdown_table("mst_department", array(), "dept_id", "dept_name", "dept_id","input[dept_id]", $dept_id);
        $data["input"]["section_name"]  	= $this->formlib->_generate_input_text("section_name", "input[section_name]", "Name", $section_name);

        $data["link_back"]	= base_url("master/section/");

        $data["title"]		= $title;
        $data["page_id"]	= "page-section-update";
        $data["_user"]		= $this->_user;
        $this->template->generate_template("master/section/update", $data);
    }
}
