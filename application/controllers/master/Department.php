<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 *
 * @property Global_model 	$global_model
 * @property Hashids 		$hashids
 * @property Formlib 		$formlib
 */

class Department extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
		!$this->access->is_login() ? redirect(base_url("auth/login/")) : "";
	}
	
	public function index()
	{
        $data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
        $data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
		$data["js"][]	= "asset/js/script.js";
		
		$data["section"]		= $this->global_model->get_data("mst_department")->result();
		$data["datatable"]		= "department";
		$data["link_update"]	= "master/department/update/";
		
		$data["title"]		= "Manage Data Department";
		$data["page_id"]	= "page-department";
		$data["_user"]		= $this->_user;
		$this->template->generate_template("master/department/index", $data);
	}
	
	public function update($id_hash = null)
	{
		$tmp_hash	= $this->hashids->decode($id_hash);
		$id			= !empty($tmp_hash[0]) ? $tmp_hash[0] : null;
		
		$data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
		$data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
		$data["js"][]	= "asset/js/script.js";
		
		if (empty($id))
		{
			$title	= "Add Data Department";
			
			$dept_id		= "";
			$dept_name	= "";
		}
		else
		{
			$edit	= $this->global_model->get_data("mst_department", array(
				"dept_id"	=> $id
			))->row();
			
			$title				= "Edit Data Department";
			$dept_id		= $this->hashids->encode($edit->dept_id);
			$dept_name	= $edit->dept_name;
		}
		
		$data["type"]				        = $this->formlib->_generate_input_text("datatable_type", "type", "datatable", "department", "hidden");
		
		$data["input"]["dept_id"]  	= $this->formlib->_generate_input_text("dept_id", "input[dept_id]", "Department ID", $dept_id , "hidden");
		$data["input"]["dept_name"]  	= $this->formlib->_generate_input_text("dept_name", "input[dept_name]", "Name", $dept_name);
		
		$data["link_back"]	= base_url("master/department/");
		
		$data["title"]		= $title;
		$data["page_id"]	= "page-department-update";
		$data["_user"]		= $this->_user;
		$this->template->generate_template("master/department/update", $data);
	}
}
