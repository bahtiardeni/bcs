<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 *
 * @property Global_model 	$global_model
 * @property Hashids 		$hashids
 * @property Formlib 		$formlib
 */

class Action extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        !$this->access->is_login() ? redirect(base_url("auth/login/")) : "";
    }

    public function index()
    {
        $data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
        $data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
        $data["js"][]	= "asset/js/script.js";

        $data["datatable"]		= "action";
        $data["link_update"]	= "master/action/update/";

        $data["title"]		= "Manage Data Action";
        $data["page_id"]	= "page-action";
        $data["_user"]		= $this->_user;
        $this->template->generate_template("master/action/index", $data);
    }

    public function update($id_hash = null)
    {
        $tmp_hash	= $this->hashids->decode($id_hash);
        $id			= !empty($tmp_hash[0]) ? $tmp_hash[0] : null;

        $data["css"][]	= "asset/template/plugins/swal/sweetalert2.css";
        //$data["css"][]	= "asset/template/plugins/timepicker/bootstrap-timepicker.min.css";
        $data["js"][]	= "asset/template/plugins/swal/sweetalert2.js";
        //$data["js"][]	= "asset/template/plugins/timepicker/bootstrap-timepicker.min.js";
        $data["js"][]	= "asset/js/script.js";

        if (empty($id))
        {
            $title	= "Add Data Action";

            $action_id	= "";
            $action_name  = "";
        }
        else
        {
            $edit	= $this->global_model->get_data("mst_action", array(
                "action_id"	=> $id
            ))->row();

            $title			= "Edit Data Action";
            $action_id	    = $this->hashids->encode($edit->action_id);
            $action_name	= $edit->action_name;
        }

        $data["type"]					= $this->formlib->_generate_input_text("datatable_type", "type", "datatable", "action", "hidden");

        $data["input"]["action_id"]  	= $this->formlib->_generate_input_text("action_id", "input[action_id]", "action ID", $action_id, "hidden");
        $data["input"]["action_name"]  	= $this->formlib->_generate_input_text("action_name", "input[action_name]", "Action Name", $action_name);

        $data["link_back"]	= base_url("master/action/");

        $data["title"]		= $title;
        $data["page_id"]	= "page-action-update";
        $data["_user"]		= $this->_user;
        $this->template->generate_template("master/action/update", $data);
    }
}
