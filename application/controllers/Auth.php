<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 *
 * @property Global_model 	$global_model
 * @property Access 		$access
 */

class Auth extends CI_Controller
{
	
	function __construct() 
	{
		parent::__construct();
	}

	function login()
	{
		$this->access->is_login() ? redirect(base_url()) : null;
		
		$data["title"]		= "Login";
		$this->load->view("auth/login", $data);
	}
	
	function ajax_login()
	{
		$data		= $this->input->post("login");
		$email		= $data["email"];
		$password	= $data["password"];
		
		$result		= $this->access->login($email, $password);
		
		echo json_encode($result);
	}
	
	function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url("auth/login"));
	}
	
	function captcha()
	{
		$this->load->library("mycaptcha");
		$this->mycaptcha->phpcaptcha('#ff0000','#fff',150,50,10,25);
		
		/*'img_width' => 150,   //Set image width.
			'img_height' => 50,   // Set image height.*/
	}
}
