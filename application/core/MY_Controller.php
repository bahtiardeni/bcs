<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	public $_user;
	public $_path_avatar;
	
	public function __construct()
    {
        parent::__construct();
        
        $this->_path_avatar	= base_url("/asset/images/avatar");
        
        $this->load->model("global_model");
        
        if (!empty($this->session->userdata("crew_id")))
        {
        	$crew_id	= $this->session->userdata("crew_id");
	
			$user		= $this->global_model->get_data("mst_crew", array(
				"crew_id"	=> $crew_id
			))->row();
	
			if (!empty($user)){
				$this->_user	= $user;
			}else{
				$this->session->sess_destroy();
				redirect(base_url("auth/login"));
			}
		}
	}
}
?>