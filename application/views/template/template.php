<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>BROADCAST CENTRE SYSTEM</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	
	<?=$_css?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="hold-transition skin-blue fixed sidebar-mini <?=$page_id;?>">
<!-- Site wrapper -->
<div class="wrapper">
	
	<?=$_header;?>
	<?=$_sidebar;?>
	<?=$_content;?>
	
	
	
	<?=$_footer;?>
</div>
<script>var base_url = "<?=base_url()?>"; </script>
<?=$_js?>
</body>
</html>
