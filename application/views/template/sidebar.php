<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<?=base_url("asset/images/avatar/".$_user->crew_avatar)?>" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p><?=$_user->crew_name?></p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		
		<!-- /.search form -->
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu" data-widget="tree">
			
			<li >
				<a href="<?=base_url()?>">
					<i class="fa fa-dashboard"></i> <span>Home</span>
				</a>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-edit"></i> <span>DATA</span>
					<span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
				</a>
				<ul class="treeview-menu">
					<li><a href="#"><i class="fa fa-book"></i> Daily Report</a></li>
					<li><a href="#"><i class="fa fa-bullhorn"></i> DO (Duty Officer)</a></li>
					<li><a href="#"><i class="fa fa-check-square-o"></i> Ceklist Live Source</a></li>
					<li><a href="#"><i class="fa fa-calendar-o"></i> Schedule Maintenance PC + <br/>Fiber Optic Mikrolink</a></li>
					<li><a href="#"><i class="fa fa-calendar-plus-o"></i> Chedule Broadcast Support</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-line-chart"></i> <span>LAPORAN</span>
					<span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
				</a>
				<ul class="treeview-menu">
					<li><a href="#"><i class="fa fa-circle-o"></i> Daily Report</a></li>
					<li><a href="#"><i class="fa fa-circle-o"></i> DO (Duty Officer)</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-files-o"></i> <span>SHARE DOCUMENTS</span>
				</a>
			
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-table"></i> <span>ADMIN (MASTER DATA)</span>
					<span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?=base_url("master/crew")?>"><i class="fa fa-users"></i> Data Crew</a></li>
					<li><a href="<?=base_url("master/department")?>"><i class="fa fa-building"></i> Data Department</a></li>
					<li><a href="<?=base_url("master/section")?>"><i class="fa fa-building-o"></i> Data Section</a></li>
					<li><a href="<?=base_url("master/shift")?>"><i class="fa fa-sliders"></i> Data Shift Schedule</a></li>
					<li><a href="<?=base_url("master/activity")?>"><i class="fa fa-tasks"></i> Data Activity</a></li>
					<li><a href="<?=base_url("master/location")?>"><i class="fa fa-map-pin"></i> Data Location</a></li>
					<li><a href="<?=base_url("master/source")?>"><i class="fa fa-plug"></i> Data Source</a></li>
					<li><a href="<?=base_url("master/ratio")?>"><i class="fa fa-cog"></i> Data Aspek Ratio</a></li>
					<li><a href="<?=base_url("master/status")?>"><i class="fa fa-sitemap"></i> Data Status</a></li>
					<li><a href="<?=base_url("master/action")?>"><i class="fa fa-cogs"></i> Data Action</a></li>
					<li><a href="<?=base_url("master/share")?>"><i class="fa fa-share"></i> Share Document</a></li>
					<li><a href="<?=base_url("master/role")?>"><i class="fa fa-user-plus"></i> Data User Role</a></li>
					<li><a href="<?=base_url("master/matrik")?>"><i class="fa fa-life-ring"></i> Data Matrik Menu</a></li>
				</ul>
			</li>
		
		
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>
