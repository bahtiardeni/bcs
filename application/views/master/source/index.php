<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="content-wrapper">
	<!-- Main content -->
	<section class="content">

        <div id="notification">
            <?php if (!empty($this->session->flashdata("message_success"))){ ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <!--<h4><i class="icon fa fa-check"></i> Alert!</h4>-->
                    <?=$this->session->flashdata("message_success")?>
                </div>
            <?php } ?>
        </div>

		<!-- Default box -->
		<div class="box">
			<div class="box-header with-border">
				<h2 class="box-title"><?=$title?></h2>
			</div>
			<div class="box-body">
				<form id="form_datatable" method="post">
					<div class="btn-group">
						<a href="<?=base_url($link_update)?>" class="btn btn-sm btn-default btn-data-add"><i class="fa fa-plus"></i> Tambah Data</a>
					</div>
					<input type="hidden" readonly name="type" id="type" value="<?=$datatable?>" />
					<input type="hidden" readonly name="link_update" value="<?=$link_update?>" />
					
					<table class="table table-bordered table-hover" style="margin-top: 10px;">
						<thead>
						<tr>
							<th><input type="text" name="search[source_name]" class="form-control input-sm" placeholder="Source Name"/></th>
							<th class="hidden-xs"><input type="text" name="search[created_on]" class="form-control input-sm" placeholder="Created"/></th>
							<th class="hidden-xs"><input type="text" name="search[created_by]" class="form-control input-sm" placeholder="Created By"/></th>
							<th class="hidden-xs"><input type="text" name="search[updated_on]" class="form-control input-sm" placeholder="Updated"/></th>
							<th class="hidden-xs"><input type="text" name="search[updated_by]" class="form-control input-sm" placeholder="Updated By"/></th>
							<th></th>
						</tr>
						<tr>
							<th>Source Name</th>
							<th class="hidden-xs text-center" style="width: 135px;">Created</th>
							<th class="hidden-xs text-center" style="width: 110px;">Created By</th>
							<th class="hidden-xs text-center" style="width: 135px;">Updated</th>
							<th class="hidden-xs text-center" style="width: 110px;">Updated By</th>
							<th style="width: 50px"></th>
						</tr>
						</thead>
						<tbody></tbody>
					</table>
					<!--<div class="row">-->
                    <div class="col-lg-4  col-md-4 col-xs-8 pull-right text-right" style="padding-right: 0px;">
                        <div id="pagination"></div>
                        <input type="hidden" id="page" name="page" value="1">
                    </div>
					<div class="col-lg-2 col-md-4 col-xs-4" style="padding-left: 0px;">
						<select name="size" id="size" aria-controls="data_table" class="form-control input-sm">
							<option value="10">10</option>
							<option value="25">25</option><option value="50">50</option>
							<option value="100">100</option>
						</select>
					</div>
					<div class="col-lg-6  col-md-4 col-xs-12" style="padding-top: 3px;">
						<span id="text_count"></span>
					</div>
				
				</form>
			</div>
		</div>
		<!-- /.box -->
	
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
