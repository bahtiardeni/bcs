<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="content-wrapper">
	<!-- Main content -->
	<section class="content">
        <div id="notification"></div>
		<!-- Default box -->
		<div class="box">
			<div class="box-header with-border">
				<h2 class="box-title"><?=$title?></h2>
			</div>
			<div class="box-body">
				<form id="form_update_datatable" method="post">
					<div class="row">
						<div class="col-lg-4">
							<?=$type?>
							<?=$input["crew_id"]?>
							<div class="form-group">
								<label>Section <span class="required">*</span></label>
								<?=$input["crew_sections_id"]?>
							</div>
							<div class="form-group">
								<label>NIK <span class="required">*</span></label>
								<?=$input["crew_nik"]?>
							</div>
							<div class="form-group">
								<label>Email <span class="required">*</span></label>
								<?=$input["crew_email"]?>
							</div>
							<div class="form-group">
								<label>Password <span class="required">*</span></label>
								<?=$input["crew_password"]?>
							</div>
							<div class="form-group">
								<label>Name <span class="required">*</span></label>
								<?=$input["crew_name"]?>
							</div>
							<div class="form-group">
								<label>HP </label>
								<?=$input["crew_hp"]?>
							</div>
							<div class="form-group">
								<label>Avatar <span class="required">*</span></label>
								<?=$input["crew_avatar"]?>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<button type="button" class="btn btn-sm btn-primary btn-save">Simpan</button>
						<a href="<?=$link_back?>" class="btn btn-sm btn-danger btn-cancel ">Batal</a>
					</div>
				</form>
			</div>
		</div>
		<!-- /.box -->
	
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
