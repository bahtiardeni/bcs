<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template 
{
	protected $_ci;
	
	protected $_js;
	protected $_css;
	protected $_user;
	
	function __construct() 
	{
		$this->_ci =& get_instance();
		
		$this->_css	= array(
			"asset/template/bower_components/bootstrap/dist/css/bootstrap.min.css",
			"asset/template/bower_components/font-awesome/css/font-awesome.min.css",
			"asset/template/bower_components/Ionicons/css/ionicons.min.css",
			"asset/template/plugins/timepicker/bootstrap-timepicker.min.css",
			"asset/template/plugins/timepicker/jquery.datetimepicker.css",
			"asset/template/dist/css/AdminLTE.min.css",
			"asset/template/dist/css/skins/_all-skins.min.css",
		    "asset/css/style.css",
		);
		
		$this->_js	= array(
			"asset/template/bower_components/jquery/dist/jquery.min.js",
			"asset/template/bower_components/bootstrap/dist/js/bootstrap.min.js",
			"asset/template/bower_components/jquery-slimscroll/jquery.slimscroll.min.js",
			"asset/template/bower_components/fastclick/lib/fastclick.js",
			"asset/template/plugins/timepicker/bootstrap-timepicker.min.js",
			"asset/template/plugins/timepicker/jquery.datetimepicker.js",
			"asset/template/dist/js/adminlte.min.js",
		);
	}
	
	function generate_template($content, $data = NULL)
	{
		$this->_user		= $data["_user"];
		$data["_css"]		= $this->generate_css($data);
		$data["_js"]		= $this->generate_js($data);
		$data["_header"]	= $this->generate_header($data);
		$data["_sidebar"]	= $this->generate_sidebar($data);
		$data["_footer"]	= $this->generate_footer($data);
		
		$data["_content"]		= $this->_ci->load->view($content, $data, TRUE);
		
		
		$this->_ci->load->view("template/template", $data);
	}
	
	protected function generate_css($data)
	{
		$content	= "";
		
		if (!empty($data["css"])){
			$this->_css	= array_merge($this->_css, $data["css"]);
		}
		
		foreach ($this->_css as $css)
		{
			$content .= "\t\t<link rel='stylesheet' media='screen'  href='".base_url($css."?v=".time())."'>" . PHP_EOL;
		}
		
		return $content;
	}
	
	protected function generate_js($data)
	{
		$content	= "";
		
		if (!empty($data["js"])){
			$this->_js	= array_merge($this->_js, $data["js"]);
		}
		
		foreach ($this->_js as $js)
		{
			$content .= "\t\t<script src='".base_url($js."?v=".time())."'></script>" . PHP_EOL;
		}
		
		return $content;
	}
	
	protected function generate_header($data)
	{
		return $this->_ci->load->view("template/header", $data, true);
	}
	
	protected function generate_sidebar($data)
	{	
		return $this->_ci->load->view("template/sidebar", $data, true);
	}
	
	protected function generate_footer($data)
	{
		return $this->_ci->load->view("template/footer", $data, true);
	}
	
}

?>