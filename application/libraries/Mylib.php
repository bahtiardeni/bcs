<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mylib
{
    private $_ci;

    function __construct()
    {
        $this->_ci =& get_instance();
    }

    function create_dir($path, $part_ori)
    {
        $dir    = ".";
        foreach ($path as $item)
        {
            $dir    .= "/".$item;
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir);
            }
        }

        return $part_ori;
    }

}