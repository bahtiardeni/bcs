<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Formlib
{
	private $_ci;
	
	function __construct()
	{
		$this->_ci =& get_instance();
	}
	
	function _generate_input_text($id = "", $name = "", $placeholder = "", $value = "", $type = "text")
	{
		$content	= "<input type='".$type."' id='".$id."' name='".$name."' class='form-control input-sm' value='".((set_value(".$name.")) ? set_value(".$name.") : $value)."' placeholder='".$placeholder."'>";
		return $content;
	}

	function _generate_input_time($id = "", $name = "", $placeholder = "", $value = "", $type = "text")
	{
		$content	= "<div class='input-group '>";
        $content	.= "<input readonly type='".$type."' id='".$id."' name='".$name."' class='form-control input-sm timepicker' value='".((set_value(".$name.")) ? set_value(".$name.") : $value)."' placeholder='".$placeholder."'>";
        $content	.= "<div class='input-group-addon'>";
        $content	.= "<i class='fa fa-clock-o'></i>";
        $content	.= "</div>";
        $content	.= "</div>";

		return $content;
	}
	
	function _generate_input_textarea($id = "", $name = "", $placeholder = "", $value = "", $type = "text")
	{
		$content	= "<textarea id='".$id."' name='".$name."' class='form-control input-sm' placeholder='".$placeholder."'>".((set_value(".$name.")) ? set_value(".$name.") : $value)."</textarea>";
		return $content;
	}
	
	function _generate_dropdown($id = "", $name = "", $list, $selected = "")
	{	
		$content = "<select id='".$id."' name='".$name."' class='form-control input-sm'>";	
		$content .= "<option value=''>Select</option>";	
		
		foreach ($list as $value => $caption)
		{
			$content .= "<option ";
			if ($selected == $value){
				$content .= " selected='selected' ";
			}
			$content .= " value='".$value."'>".$caption."</option>";
		}
		
		$content .= "</select>";
		return $content;
	}
	
	function _generate_dropdown_array($id = "", $name = "", $list, $selected = "")
	{
		$content = "<select id='".$id."' name='".$name."' class='form-control input-sm'>";
		$content .= "<option value=''>Select</option>";
		
		foreach ($list as $item)
		{
			$content .= "<option ";
			if ($selected == $item){
				$content .= " selected='selected' ";
			}
			$content .= " value='".$item."'>".$item."</option>";
		}
		
		$content .= "</select>";
		return $content;
	}
	
	function _generate_input_file_image($id = "", $name = "", $value = "", $path = "")
	{
		$content	= "
		<input class='target_image' type='hidden' id='".$id."' name='".$name."' value='".((set_value(".$name.")) ? set_value(".$name.") : $value)."'>
		<input type='file' accept='image/*' class='textbox-file hidden' name='files' >
		<div class='file-images'> 
		".(!empty($value) ? "<img src='".($path."/".$value)."' style='width: 50%; margin-bottom: 5px; border: 1px solid #ccc;;' />" : "")."
		</div>
		<button type='button' class='btn btn-xs btn-success btn-upload-file'>Upload</button>
		".(!empty($value) ? "" : "")."
		";
		
		return $content;
	}

	function _generate_dropdown_table($table_name, $condition = array(), $field_value = "id", $field_caption = "name", $id = "", $name = "", $selected = "", $where_in = array(), $order_field = "", $order_type = "ASC")
	{
		if ($table_name == "mst_menu"){
			$order_field = "order";
		}else if (empty($order_field)){
			$order_field = $field_caption;
		}
		
		$list_dropdown	= $this->_ci->global_model->get_data($table_name, $condition, $order_field, $order_type, null, null, array(), array(), $where_in)->result();
		
		//get_data($table_name = "", $condition = array(), $order_field = "", $order_type = "ASC", $page = null, $data_size = null, $field_select = array(), $table_join = array(), $where_in = array())
		
		$content = "<select id='".$id."' name='".$name."' class='form-control input-sm select-autocomplete'>";	
		$content .= "<option value=''>Select</option>";		
		foreach ($list_dropdown as $item_dropdown)
		{
			$content .= "<option ";
			if ($selected == $item_dropdown->$field_value){
				$content .= " selected='selected' ";
			}
			
			if ($table_name == "mst_akun")
			{
				$caption	= $item_dropdown->kodeakun." - ".$item_dropdown->namaakun;
			}
			else
			{
				$caption	= $item_dropdown->$field_caption;
			}
			
			$content .= " value='".$item_dropdown->$field_value."'>".$caption."</option>";
			
			
			if ($table_name == "mst_menu")
			{
				$childs	= $this->_ci->global_model->get_data($table_name, array(
					"id_parent" => $item_dropdown->id
				), ($table_name == "mst_menu" ? "order" : $field_caption))->result();
				
				foreach ($childs as $child)
				{
					$caption	= $child->$field_caption;
					
					$content .= "<option ";
					if ($selected == $child->$field_value){
						$content .= " selected='selected' ";
					}
					
					$content .= " value='".$child->$field_value."' style='padding-left: 20px;'>".$caption."</option>";
				}
				
			}
			
		}
		$content .= "</select>";
		return $content;
	}

	function _generate_switch($id = "", $name = "", $value = 1)
	{
		$content	= "<input type='hidden' name='".$name."' value='0'>";
		$content	.= "<input type='checkbox' id='".$id."' name='".$name."' ".($value ? "checked" : "")." class='bs_switch' data-size='small' value='1'>";
		
		return $content;
	}

	function _generate_input_checkbox($id = "", $name = "", $caption = "", $value = "")
	{
		$content	= "<input type='hidden' name='".$name."' value='0'>";
		$content	.= "<input type='checkbox' name='".$name."' ".($value ? "checked" : "")." value='1'>&nbsp; ".$caption;
		return $content;
	}
	
	
	function _generate_dropdown_akun($id = "", $name = "", $selected = "")
	{
		$content = "<select id='".$id."' name='".$name."' class='form-control input-sm select2'>";
		$content .= "<option value=''  selected='selected'>Select</option>";
		
		
		$lists		= $this->_ci->global_model->get_data("mst_akun")->result();
		
		foreach ($lists as $list)
		{
			$content .= "<option ".($list->klasifikasi_id == $selected ? "selected='selected'" : "" )." value='".$list->akun_id."'>".$list->kodeakun." - ".$list->namaakun."</span></option>";
		}
		
		$content .= "</select>";
		return $content;
	}
	
}