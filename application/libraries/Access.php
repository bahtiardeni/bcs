<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Access 
{
	protected $_ci;
	
	function __construct() 
	{
		$this->_ci =& get_instance();
		
		$this->_ci->load->helper('email');
		$this->_ci->load->model('global_model');
		
	}
	
	function login($email = "", $password = "")
	{
		$result		= "error";
		$message	= "";
		
		if (empty($email) && empty($password))
		{
			$message	= "Email atau Password tidak boleh kosong.";
		}
		else
		{
			$user	= $this->_ci->global_model->get_data("mst_crew", array(
				"crew_email"		=> $email,
				"crew_password"		=> md5($password),
			))->row();
			
			
			if (empty($user)){
				$message	= "Email dan Password tidak benar.";
			}else{
				$result		= "success";
				
				$this->_ci->session->set_userdata(array(
					"crew_id"	=> $user->crew_id
				));
			}
			
		}
		
		/*echo($message);
		die();*/
		
		return array(
			"result"	=> $result,
			"message"	=> $message
		);
	}
	
	function is_login()
	{
		if($this->_ci->session->userdata("crew_id"))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
?>