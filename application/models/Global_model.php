<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Global_model extends CI_Model 
{
	
	function __construct() 
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
	}
	
	function get_data($table_name = "", $condition = array(), $order_field = "", $order_type = "ASC", $page = null, $data_size = null, $field_select = array(), $table_join = array(), $where_in = array())
	{
		if (mysqli_more_results($this->db->conn_id)) {
            mysqli_next_result($this->db->conn_id);
        }
		$data	= $this->db;
		
		if (!empty($field_select))
		{
			foreach ($field_select as $select)
			{
				$data->select($select);
			}
		}
		
		$data->from($table_name);
		
		if (!empty($table_join))
		{
			foreach ($table_join as $type => $joins)
			{
				foreach ($joins as $join_table => $join_on)
				{
					$data->join($join_table, $join_on, ($type != "join" ? $type : ""));
				}
			}
		}
		
		foreach ($condition as $field => $value)
		{
			$data->where($field, $value);
		}
		
		foreach ($where_in as $field => $value)
		{
			$this->db->where_in($field, $value);
		}
		
		if (!empty($order_field) && !empty($order_type)){
			$data->order_by($order_field, $order_type);
		}
		
		if ((!empty($page) || $page == 0 ) && !empty($data_size)){
			$data->limit((int)$data_size, $page);
		}
		
		$get	= $data->get();
		
		return $get;
	}
	
	function get_count($table_name = "", $condition = array())
	{
		if (mysqli_more_results($this->db->conn_id)) {
            mysqli_next_result($this->db->conn_id);
        }
		$data	= $this->db;
		$data->from($table_name);
		$data->select("COUNT(id) as 'total'");
		
		foreach ($condition as $field => $value)
		{
			$data->where($field, $value);
		}
		
		$get	= (int)$data->get()->row()->total;
		
		return $get;
	}

	function save($table_name, $data_table)
	{
		$data = $this->db;
		
		$data->insert($table_name, $data_table);
		return $data->insert_id();		
	}
	
	function save_batch($table_name, $data_table)
	{
		$data = $this->db;
		
		return $data->insert_batch($table_name, $data_table);
	}
	
	function update($table_name, $condition, $data_update)
	{
		$data = $this->db;
		
		foreach ($condition as $field => $value)
		{
			$data->where($field, $value);
		}
		
		$update	= $data->update($table_name, $data_update);
		return $update;
	}
	
	function delete($table_name, $condition)
	{
		$data = $this->db;
		
		foreach ($condition as $field => $value)
		{
			$data->where($field, $value);
		}
		return $data->delete($table_name);
	}
	
	function get_query($query)
	{
		$data = $this->db;
		$get	= $data->query($query);
		
		return $get;
	}
}
?>