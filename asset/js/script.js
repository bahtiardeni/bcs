/**
 * Created by bahtiardeni on 17/04/2018.
 */

var JBcs = function () {
    var ajaxDataTable, ajax;
    var image_loading 	= "<center class='image-loading'><img src='" + base_url + "asset/images/loading.gif' style='width: 32px; margin: 20px;' /></center>";

    var handlePageLogin = function(){
        var pageElement = $(".login-page");

        if (pageElement.length)
        {
            pageElement.on("click",".btn-login", function(){
                handleAjaxLogin(pageElement);
            })

            pageElement.on("click",".refresh-captcha", function(){
                handleRefreshCaptcha();
            })

            pageElement.keypress(function(e) {
                if(e.which == 13) {
                    handleAjaxLogin(pageElement);
                }
            });

            pageElement.on("click", "#show_password", function(){
                if ($(this).is(':checked')) {
                    $("#password").attr("type", "text")
                } else {
                    $("#password").attr("type", "password")
                }
            });
        }
    }

    var handleAjaxLogin = function(element){
        $.ajax({
            url: base_url + "auth/ajax_login",
            type: 'POST',
            dataType : "JSON",
            beforeSend: function() {
                handleClearNotification();
                element.find("input, button").attr("disabled", true);
                element.find(".btn-login").text("Please wait");
            },
            data: element.find("form").serialize(),
            success: function (data) {

                if (data.result == "success"){
                    element.submit();
                    location.reload();
                }else{
                    handleCreateNotification("error", "Error", data.message);
                }

                element.find("input, button").attr("disabled", false);
                element.find(".btn-login").text("Login");
            },
            error: function (request, status, error) {
                element.find("input, button").attr("disabled", false);
                element.find(".btn-login").text("Login");
            }
        });
    }

    var handleCreateNotification = function (type, title, message) {

        var content = "";

        if (type == "error"){
            content = "<div class='alert alert-danger alert-dismissible'>";
            content += "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>";
            content += "<h4><i class='icon fa fa-ban'></i> "+title+"</h4>";
            content += message;
            content += "</div>";
        }
        else if (type == "success"){
            content = "<div class='alert alert-success alert-dismissible'>";
            content += "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>";
            content += "<h4><i class='icon fa fa-ban'></i> "+title+"</h4>";
            content += message;
            content += "</div>";
        }

        console.log(content);

        $("#notification").html(content);
    }

    var handleClearNotification = function(){
        $("#notification").html("");
    }

    // Datatable
    var handleDataTable = function(){
        var element	= $("#form_datatable");

        if (element.length)
        {
            var colspan = 0;
            if ($("#type").val() == "realisasi"){
                colspan = 18;
            }else{
                colspan = element.find( "table thead tr:nth-child(1) th" ).length ;
            }

            if(ajaxDataTable) ajaxDataTable.abort();
            ajaxDataTable = $.ajax({
                type: "GET",
                dataType : "JSON",
                url: base_url + 'ajax/datatable/',
                beforeSend : function(){
                    element.find("table tbody").html("<tr><td colspan='" + colspan + "' style='background-color: #fff'>" + image_loading + "</td></tr>");
                },
                data : element.serialize(),
                success: function (data) {

                    var table_content	= "";

                    var class_hidden_xs     = data.class["hidden-xs"];
                    var class_text_center   = data.class["text-center"];
                    var class_text_right    = data.class["text-right"];


                    $.each(data.datatable, function(key, row_data) {

                        table_content += "<tr>";

                        $.each(row_data, function(key_row, row) {

                            var class_td = "";

                            if(jQuery.inArray(key_row, class_hidden_xs) !== -1){
                                class_td += " hidden-xs";
                            }

                            if(jQuery.inArray(key_row, class_text_center) !== -1){
                                class_td += " text-center";
                            }

                            if(jQuery.inArray(key_row, class_text_right) !== -1){
                                class_td += " text-right";
                            }


                            table_content += "<td class='"+class_td+"'>" + row + "</td>";
                        });

                        table_content += "</tr>";
                    });

                    if (data.datatable.length == 0)
                    {
                        table_content	= "<tr><td colspan='" + colspan + "' style='background-color: #fff; text-align: center; color: #ff0000; padding: 10px;'><b>Data tidak ditemukan.</b></td></tr>";
                    }



                    if ($("#type").val() == "realisasi"){

                        handleClearNotification();

                        if ($("#eselon_id").val() == "" || $("#satker_id").val() == "")
                        {
                            table_content	= "<tr><td colspan='" + colspan + "' style='background-color: #fff; text-align: center; color: #ff0000; padding: 10px;'><b>Silahkan pilih dulu Eselon dan Satuan Kerja.</b></td></tr>";

                            element.find("table tbody").html(table_content);

                            element.find(".btn-save").addClass("hidden");
                        }
                        else
                        {
                            element.find("table tbody").html(table_content);
                            element.find("#text_count").text("Total data : " + data.count);

                            $(".number").number(true, 0);
                            $(".number").addClass("text-right");
                            $(".number").attr("onfocus", "this.select();");

                            handleTotalRealisasi();
                            element.find(".btn-save").removeClass("hidden");
                        }
                    }
                    else
                    {
                        element.find("table tbody").html(table_content);
                        element.find("#text_count").text("Total data : " + data.count);
                    }



                    handlePagnation(element, parseInt(element.find("#page").val()), parseInt($("#size").val()), data.count);
                    handleFilterDataTable();
                },
                error: function (jqXHR, exception) {

                },
            });
        }
    }

    var handlePagnation	= function(element, paged, size, count){

        var total_page = Math.ceil(count/size);

        var content	= "";

        if (total_page >= 1)
        {

            content	+= "<ul class='pagination pagination-sm no-margin pull-right'>";
            content	+= "<li class='paginate_button previous " + (paged == 1 ? "disabled" : "") + " id='example1_previous' data-page='" + (paged - 1) + "'><a href='javascript:void(0)' >Previous</a></li>";
            content	+= "<li class=' active'><a href='javascript:void(0)'>" + paged + "</a></li>";
            content	+= "<li class='paginate_button next " + (paged == total_page ? "disabled" : "") + "' id='example1_next' data-page='" + (paged + 1) + "'><a href='javascript:void(0)' >Next</a></li>";
            content	+= "</ul>";
        }

        element.find("#pagination").html(content);
    }

    var handleFilterDataTable = function(){
        var element_id	= "#form_datatable";

        $(document).on("click", ".btn-refresh", function (e) {
            handleDataTable();
        });

        $(document).on("keyup", element_id + " input", function (e) {

            if (!$(this).hasClass("textbox-realisasi")){
                $("#page").val(1);
                handleDataTable();
            }

        });

        $(document).on("change", element_id + " select", function (e) {

            if ($(this).attr("id") != "eselon_id")
            {
                $("#page").val(1);
                handleDataTable();
            }
            else
            {
                var eselon_id = $(this).val();

                //alert(eselon_id);

                $.ajax({
                    url: base_url + "ajax/get_satker",
                    dataType : "json",
                    type: "POST",
                    data: {
                        eselon_id : eselon_id
                    },
                    beforeSend : function(){
                        $("#satker_id").html("<option>Silahkan tunggu...</option>");
                        $("#satker_id").attr("disabled", true);
                    },
                    success: function (data) {

                        var content	= "<option value=''>Pilih</option>";

                        $.each(data, function(key, row_data) {
                            content	+= "<option value='"+row_data.satker_id+"'>"+row_data.namasatker+"</option>";
                        });


                        $("#satker_id").html(content);
                        $("#satker_id").attr("disabled", false);

                        $("#page").val(1);
                        handleDataTable();
                    }
                });
            }


        });

        $(document).on("click", element_id + " table thead tr th.sort", function (e) {

            var sort_field	= $(this).data("sort");

            $(element_id + " table thead tr th.sort span").html("");

            if ($(this).hasClass("sort-asc")){
                $(element_id + " table thead tr th.sort").removeClass("sort-asc").removeClass("sort-desc");
                $(this).addClass("sort-desc");
                $(this).find("span").html(sort_desc);
                $(element_id + " #sort_type").val("desc");
            }else{
                $(element_id + " table thead tr th.sort").removeClass("sort-asc").removeClass("sort-desc");
                $(this).addClass("sort-asc");
                $(this).find("span").html(sort_asc);
                $(element_id + " #sort_type").val("asc");
            }

            $(element_id + " #sort_name").val(sort_field);
            $(element_id + " #page").val(1);
            handleDataTable();
        });

        $(document).on("click", element_id + "  .paginate_button", function (e) {

            if (!$(this).hasClass("disabled"))
            {
                $(element_id + " #page").val($(this).data("page"));

                handleDataTable();
            }
        });

        $(document).on('click', '#form_datatable .btn-remove-data', function () {

            handleClearNotification();

            var el = $(this);

            swal({
                title: "Apakah Anda yakin?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya"
            }).then(function () {
                $.ajax({
                    url: base_url + 'ajax/datatable_delete/',
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        type : $("#type").val(),
                        id : el.data("id")
                    },
                    success: function (res) {
                        if (res.result == "success"){
                            handleDataTable();
                            handleCreateNotification("success", "Success", res.message);
                        }else{
                            handleNotificationError(res.message);
                        }
                    },
                    error: function (jqXHR, exception) {

                    }
                });
            }, function (dismiss) {

            });
        });

        $(document).on('click', '#form_datatable .btn-reset-password', function () {

            handleClearNotification();

            var el = $(this);

            swal({
                title: "Apakah Anda yakin?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya"
            }).then(function () {
                $.ajax({
                    url: base_url + 'ajax/reset_password/',
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        type : $("#type").val(),
                        id : el.data("id")
                    },
                    success: function (res) {
                        if (res.result == "success"){
                            swal(
                                'Password berhasil direset!',
                                '',
                                'success'
                            )
                        }else{
                            handleNotificationError(res.message);
                        }
                    },
                    error: function (jqXHR, exception) {

                    }
                });
            }, function (dismiss) {

            });
        });
    }

    var handleUpdateDataTable = function(){
        var element	= $("#form_update_datatable");

        if (element.length)
        {
            element.on("click", ".btn-save", function () {

                handleClearNotification();

                swal({
                    title: "Apakan Anda yakin?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#337ab7",
                    confirmButtonText: "Yes"
                }).then(function () {
                    $.ajax({
                        url: base_url + "ajax/datatable_update",
                        type: "POST",
                        dataType: "JSON",
                        data: $("#form_update_datatable").serialize(),
                        beforeSend : function(){
                            $("#form_update_datatable input, textarea, select, button").attr("disabled", true);
                            $("#form_update_datatable .btn-save").text("Silahkan tunggu...");
                        },
                        success: function (res) {
                            if (res.result == "success"){
                                window.location	= $("#form_update_datatable .btn-cancel").attr("href");
                            }else{
                                handleCreateNotification("error", "Error", res.message);
                            }

                            $("#form_update_datatable input, textarea, select, button").attr("disabled", false);
                            $("#form_update_datatable .btn-save").text("Simpan");
                        },
                        error: function (jqXHR, exception) {
                            $("#form_update_datatable input, textarea, select, button").attr("disabled", false);
                            $("#form_update_datatable .btn-save").text("Simpan");
                        },
                    });
                }, function (dismiss) {

                });
            });

            element.on("click", ".btn-upload-file", function () {
                $(this).parent().find(".textbox-file").click();
            });

            element.on("change", ".textbox-file", function () {
                var el = $(this);

                var file_data = el.prop('files')[0];
                var form_data = new FormData();
                form_data.append('file', file_data);

                el.parent().find(".file-images img").attr("src", base_url + "/asset/images/loading.gif");

                $.ajax({
                    url: base_url + "/ajax/upload_images/avatar",
                    type: "POST",
                    dataType: "JSON",
                    data: form_data,
                    processData: false,
                    contentType: false,
                    success: function (res) {
                        if (res.result == "success") {

                            console.log(el.parent().find(".target_images").length);

                            el.parent().find(".target_image").val(res.file);
                            el.parent().find(".file-images img").attr("src", res.path);

                        }
                    }
                });

            });

            if ($(".timepicker").length)
            {
                $(function() {
                    //$('.timepicker').timepicker({ 'timeFormat': 'H:i:s' });
                    $('.timepicker').datetimepicker({
                        datepicker:false,
                        format:'H:i:s',
                        step:5,
                        onSelectTime:function (current_time,$input) {

                            var value   = $input.val();
                            value       = value.split(":");
                            value       = value[0]+":"+value[1]+":00";

                            $input.val(value);
                        }
                    });
                });
            }



            /*$('.timepicker').timepicker({
                timeFormat: 'h:mm p',
                interval: 60,
                minTime: '10',
                maxTime: '6:00pm',
                defaultTime: '11',
                startTime: '10:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            })*/
        }

        // Dropdowntree
        if ($(".dropdown-tree").length)
        {
            var element_target	= $(".dropdown-tree").attr("data-target");

            var options = {
                title : (klasifikasi == "" ? "Select" : klasifikasi),
                data: list_klasifikasi,
                maxHeight: 3000,
                selectChildren : true,
                clickHandler: function(element){
                    $("#dropdowntree").SetTitle($(element).find("a").first().text());
                    $("#" + element_target).val($(element).attr("data-value"));
                    $("#" + element_target).trigger("change");

                    var el_caret = $(element).find("a span i").first();

                    if (el_caret.hasClass("fa-caret-right")){
                        el_caret.addClass('fa-caret-down');
                        el_caret.removeClass('fa-caret-right');
                    }else{
                        el_caret.addClass('fa-caret-right');
                        el_caret.removeClass('fa-caret-down');
                    }

                    var el_list = $(element).find("ul").first();

                    if (el_list.css('display') == 'none'){
                        el_list.show();
                    }else{
                        el_list.css('display','none');
                    }
                },
                closedArrow: '<i class="fa fa-caret-right" aria-hidden="true"></i>',
                openedArrow: '<i class="fa fa-caret-down" aria-hidden="true"></i>',
                multiSelect: false,
            }


            $("#dropdowntree").DropDownTree(options);
        }
    }
    // Datatable

    return {
        initializeJBcs: function () {
            handlePageLogin();
            handleDataTable();
            handleUpdateDataTable();
        }
    };
}();

$(document).ready(function(){
    JBcs.initializeJBcs();
});